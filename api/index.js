const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const config = require('./config');
const app = express();
require('express-ws')(app);

const users = require('./app/users');
const User = require("./models/User");
const Message = require("./models/Message");

app.use(express.json());
app.use(cors());

const port = 8000;

app.use('/users', users);

const clients = {};

const broadcast = message => {
  Object.values(clients).forEach(client => {
    client.connection.send(JSON.stringify(message));
  })
};

const createMessage = async (text, user) => {
  const message = new Message({
    text,
    user: user._id
  });

  await message.save();

  message.user = user;

  broadcast({
    type: 'NEW_MESSAGE',
    message
  })
};

const sendLastMessages = async (ws) => {
  const lastMessages = await Message.find().populate('user').sort({datetime: -1}).limit(30);
  ws.send(JSON.stringify({
    type: 'LAST_MESSAGES',
    messages: lastMessages
  }))
};

const sendLoggedInUsers = (ws) => {
  const users = Object.values(clients).map(client => client.user);
  ws.send(JSON.stringify({
    type: 'LOGGED_IN_USERS',
    users
  }));
};

const configureConnection = (ws, req) => {
  const id = req.user._id;
  const user = req.user;

  clients[id] = {connection: ws, user};

  sendLoggedInUsers(ws);
  sendLastMessages(ws);

  broadcast({
    type: 'USER_LOGGED_IN',
    user
  });

  ws.on('message', async (msg) => {
    let decodedMessage;

    try {
      decodedMessage = JSON.parse(msg);
    } catch (e) {
      return ws.send(JSON.stringify({
        type: 'ERROR',
        message: 'Message is not JSON'
      }));
    }

    switch (decodedMessage.type) {
      case 'CREATE_MESSAGE':
        createMessage(decodedMessage.message, user);
        break;
      default:
        return ws.send(JSON.stringify({
          type: 'ERROR',
          message: 'Unknown message type'
        }));
    }

  });

  ws.on('close', () => {
    delete clients[id];
    broadcast({
      type: 'USER_LOGGED_OUT',
      user
    })
  });
};

app.ws('/messenger', async (ws, req) => {
  const token = req.query.token;

  const user = await User.findOne({token});

  if(!user) {
    ws.send(JSON.stringify({type: 'ERROR', message: 'Not authenticated'}));
    return ws.close();
  }

  req.user = user;

  configureConnection(ws, req);
});

const run = async () => {
  await mongoose.connect(config.db.url);

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });

  exitHook(() => {
    console.log('exiting');
    mongoose.disconnect();
  })
};

run().catch(e => console.error(e));
