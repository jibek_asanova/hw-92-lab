import Layout from "./components/UI/Layout/Layout";
import {Switch, Route, Redirect} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Chat from "./containers/Chat/Chat";
import {useSelector} from "react-redux";
import MainPage from "./containers/Main/MainPage";

const App = () => {

    const user = useSelector(state => state.users.user);

    const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
      return isAllowed ?
          <Route {...props}/> :
          <Redirect to={redirectTo}/>
    };
    return (
        <Layout>
            <Switch>
                <ProtectedRoute
                    path="/messenger"
                    component={Chat}
                    isAllowed={user}
                    redirectTo="/login"
                />
                <Route path="/messenger" exact component={Chat}/>
                <Route path="/register" component={Register}/>
                <Route path="/login" component={Login}/>
                <Route path="/" component={MainPage}/>
            </Switch>
        </Layout>

    );
};

export default App;
