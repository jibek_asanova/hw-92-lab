import React, {useEffect, useRef, useState} from 'react';
import ChatLayout from "../../components/UI/Layout/ChatLayout";
import {useSelector, useDispatch} from "react-redux";
import {
    fetchNewMessage,
    fetchAllMessages,
    fetchLoggedInUsers,
    fetchLoggedInUser,
} from "../../store/actions/chatActions";
import {Button, Grid, makeStyles, TextField} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    button: {
        marginLeft: theme.spacing(2),
    }
}))

const Chat = () => {
    const classes = useStyles();

    const dispatch = useDispatch();
    const [message, setMessage] = useState('');
    const ws = useRef(null);
    const user = useSelector(state => state.users.user);
    const messages = useSelector(state => state.chat.messages);


    useEffect(() => {
        ws.current = new WebSocket(`ws://localhost:8000/messenger?token=${user.token}`);

        ws.current.onmessage = (message) => {

            const decodedMessage = JSON.parse(message.data);

            switch (decodedMessage.type) {
                case 'NEW_MESSAGE':
                    dispatch(fetchNewMessage(decodedMessage.message));
                    break;
                case 'LOGGED_IN_USERS':
                    dispatch(fetchLoggedInUsers(decodedMessage.users));
                    break;
                case 'LAST_MESSAGES':
                    dispatch(fetchAllMessages(decodedMessage.messages));
                    break;
                case 'USER_LOGGED_IN':
                    dispatch(fetchLoggedInUser(decodedMessage.user));
                    break;
                default:
                    console.log('Unknown message: ', decodedMessage);
            }
        };

        ws.current.onopen = () => {
            console.log('Connection established');
        };

    }, [dispatch, user]);

    const sendMessage = () => {
        ws.current.send(JSON.stringify({
                type: 'CREATE_MESSAGE',
                message
            }
        ));

        setMessage('');
    };


    return (
        <ChatLayout>
            <div>
                <Grid container direction='row' alignItems='center' component='form'>
                    <Grid item xs>
                        <TextField
                            type="text"
                            value={message}
                            onChange={e => setMessage(e.target.value)}
                            required
                        />
                    </Grid>
                    <Grid item xs>
                        <Button type="submit" color="primary" variant="contained" onClick={sendMessage} className={classes.button}>
                            Send message
                        </Button>
                    </Grid>
                </Grid>

                <div>
                    {messages && messages.map((message, i) => (
                        <p key={i}><b>{message.user.username}</b>: {message.text}</p>
                    ))}
                </div>
            </div>
        </ChatLayout>
    );
};

export default Chat;