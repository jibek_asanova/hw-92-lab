import React from 'react';
import {Typography} from "@material-ui/core";

const MainPage = () => {
    return (
        <Typography component="h1">
            Для того чтобы переписываться , пожалуйста залогиньтесь!
        </Typography>
    );
};

export default MainPage;