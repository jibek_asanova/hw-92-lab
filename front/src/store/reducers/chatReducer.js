import {
    FETCH_ALL_MESSAGES,
    FETCH_LOGGED_IN_USER,
    FETCH_LOGGED_IN_USERS,
    FETCH_NEW_MESSAGE
} from "../actions/chatActions";

const initialState = {
    messages: [],
    users: []
};

const chatReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_NEW_MESSAGE:
            return {...state, messages: [...state.messages, action.message]};
        case FETCH_ALL_MESSAGES:
            return {...state, messages: [...state.messages, ...action.messages]};
        case FETCH_LOGGED_IN_USERS:
            return {...state, users: action.users};
        case FETCH_LOGGED_IN_USER:
            const newUser = action.user;
            const userIndex = state.users.findIndex(user => user._id === newUser._id);

            if (userIndex === -1) {
                return {...state, users: [...state.users, action.user]}
            }
            return {...state};
        default:
            return state;
    }
};

export default chatReducer;