export const FETCH_NEW_MESSAGE = 'FETCH_NEW_MESSAGE';
export const FETCH_ALL_MESSAGES = 'FETCH_ALL_MESSAGES';
export const FETCH_LOGGED_IN_USERS = 'FETCH_LOGGED_IN_USERS';
export const FETCH_LOGGED_IN_USER = 'FETCH_LOGGED_IN_USER';

export const fetchNewMessage = message => ({
    type: FETCH_NEW_MESSAGE, message
});

export const fetchAllMessages = messages => ({
    type: FETCH_ALL_MESSAGES, messages
});

export const fetchLoggedInUsers = users => ({
    type: FETCH_LOGGED_IN_USERS, users
});

export const fetchLoggedInUser = user => ({
    type: FETCH_LOGGED_IN_USER, user
});

