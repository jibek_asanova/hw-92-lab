import React from 'react';
import {AppBar, Grid, makeStyles, MenuItem, Toolbar, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import Anonymous from "./Menu/Anonymous";
import {logoutUser} from "../../../store/actions/usersActions";

const useStyles = makeStyles(theme => ({
    mainLink: {
        color: "inherit",
        textDecoration: 'none',
        '$:hover': {
            color: 'inherit'
        }
    },
    staticToolbar: {
        marginBottom: theme.spacing(2)
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
    },
}));

const AppToolbar = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);

    return (
        <>
            <AppBar position="fixed" className={classes.appBar}>
                <Toolbar>
                    <Grid container justifyContent="space-between" alignItems="center">
                        <Typography variant="h6">
                            <Link to="/" className={classes.mainLink}>Messenger</Link>
                        </Typography>
                        <Grid item>
                            {user ? (
                                <Grid container>
                                    <p>Hello, {user.username}</p>
                                    <MenuItem onClick={() => dispatch(logoutUser())}>Logout</MenuItem>
                                </Grid>
                            ) : (
                                <Anonymous/>
                            )}

                        </Grid>

                    </Grid>
                </Toolbar>
            </AppBar>
            <Toolbar className={classes.staticToolbar}/>
        </>
    );
};

export default AppToolbar;